export interface EarnModel {
    name: string;
    lastName: string;
    earned: Number
}