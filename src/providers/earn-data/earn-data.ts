import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { EarnModel } from '../../models/earn-model';

// application configuration data.
import { AppConfig } from '../../app.config';

@Injectable()
export class EarnDataProvider {

  constructor(private http: HttpClient) {
    
  }

  getEarnedData() {
    return this.http.get<EarnModel>(AppConfig.apiUrl);
  }

}
