import { Component } from '@angular/core';

@Component({
  selector: 'slider',
  templateUrl: 'slider.html'
})
export class SliderComponent {

  sliderHeaderText: string;

  constructor() {
    this.sliderHeaderText = 'Choose your favorite game';
  }

}
