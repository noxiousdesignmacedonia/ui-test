import { Component, Input } from '@angular/core';
import { EarnDataProvider } from '../../providers/earn-data/earn-data';

@Component({
  selector: 'list',
  templateUrl: 'list.html'
})
export class ListComponent {

  @Input() headerText: string;
  listData: any;
  expanded: boolean = false;
  selectedListItem: any = {name:'None', lastName: 'Selected', earned: 0};  

  constructor(
    private earnDataProvider: EarnDataProvider
  ) {

  }

  selectListItem(item):void {
    this.selectedListItem = item;
    this.toggleDropDown();
  }

  toggleDropDown():void {
    if (this.listData == null) this.getListData();
    this.expanded = !this.expanded;
  }

  getListData():void {
    this.earnDataProvider.getEarnedData().subscribe(
      res => this.listData = res
    );
  }
}
