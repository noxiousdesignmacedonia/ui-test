import { Component } from '@angular/core';
import { Input } from '@angular/core';

/**
 * Generated class for the SocialComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'social',
  templateUrl: 'social.html'
})
export class SocialComponent {

  @Input() headerText: string;

  constructor() {

  }

}
