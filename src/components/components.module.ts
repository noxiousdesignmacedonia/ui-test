import { NgModule } from '@angular/core';
import { SliderComponent } from './slider/slider';
import { ListComponent } from './list/list';
import { SocialComponent } from './social/social';
@NgModule({
	declarations: [SliderComponent,
    ListComponent,
    SocialComponent],
	imports: [],
	exports: [SliderComponent,
    ListComponent,
    SocialComponent]
})
export class ComponentsModule {}
